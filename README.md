# dotfiles

To be managed with GNU stow. Install all dotfiles:

```bash
~/dotfiles> stow env
~/dotfiles> stow git
~/dotfiles> stow fish
~/dotfiles> stow nvim
```
