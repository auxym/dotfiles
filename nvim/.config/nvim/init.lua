
require "paq" {
    "savq/paq-nvim";                  -- Let Paq manage itself

    "b0o/mapx.nvim";
    "tpope/vim-sleuth";
    "ojroques/nvim-hardline";
    "phaazon/hop.nvim";
    "vimwiki/vimwiki";
    "echasnovski/mini.nvim";
    "L3MON4D3/LuaSnip";

    -- Telescope
    "nvim-lua/plenary.nvim";
    "nvim-telescope/telescope.nvim";
    "nvim-telescope/telescope-frecency.nvim";
    "tami5/sqlite.lua"; -- Required for telescope-frecency
    "kyazdani42/nvim-web-devicons";
    "vimwiki/vimwiki";
    
    --cmp
    "hrsh7th/cmp-nvim-lsp";
    "hrsh7th/cmp-buffer";
    "hrsh7th/cmp-path";
    "hrsh7th/cmp-cmdline";
    "hrsh7th/nvim-cmp";

    -- Colorschemes
    "rafamadriz/neon";
    "shaeinst/roshnivim-cs";
    "neovim/nvim-lspconfig";
}

-- aliases
local opt  = vim.opt     -- global
local g  = vim.g     -- global for let options
local wo = vim.wo    -- window local
local bo = vim.bo    -- buffer local
local fn = vim.fn    -- access vim functions
local cmd = vim.cmd  -- vim commands

function _G.ReloadConfig()
  for name,_ in pairs(package.loaded) do
    if name:match('^cnull') then
      package.loaded[name] = nil
    end
  end
  dofile(vim.env.MYVIMRC)
end

-- ===========================================================================
--            General Options
-- ===========================================================================

opt.smartcase = true
opt.ignorecase = true
opt.hlsearch = true
opt.incsearch = true
opt.showcmd = true
opt.wildmenu = true
opt.mouse = 'a'
opt.textwidth = 100
opt.fileformats = {"unix", "dos"}

opt.wrap = false
opt.expandtab = true
opt.softtabstop = 4
opt.shiftwidth = 4
opt.number = true

opt.completeopt = {"menu", "menuone", "noselect"}

-- ===========================================================================
--            Key Maps
-- ===========================================================================

g.mapleader = ","

local mapx = require'mapx'
if mapx.setup ~= true then mapx.setup{ global = true } end

map("<leader>hi", function() print("world") end, "silent")
map("<leader>rc", function() ReloadConfig() end, "silent")
nnoremap("<CR>", ":noh<CR><CR>", "silent")

-- Navigate between windows with arrow keys
nnoremap("<C-Right>", "<c-w>l", "silent")
nnoremap("<C-Left>", "<c-w>h", "silent")
nnoremap("<C-Up>", "<c-w>k", "silent")
nnoremap("<C-Down>", "<c-w>j", "silent")

-- Telescope
nnoremap("<leader>ff", function() require('telescope.builtin').find_files() end)
nnoremap("(<leader>fg", function() require('telescope.builtin').live_grep() end)
nnoremap("<leader>fb", function() require('telescope.builtin').buffers() end)
nnoremap("<leader>fh", function() require('telescope.builtin').help_tags() end)

-- hop maps
vim.api.nvim_set_keymap('n', 'f', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>", {})
vim.api.nvim_set_keymap('n', 'F', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>", {})
vim.api.nvim_set_keymap('o', 'f', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true, inclusive_jump = true })<cr>", {})
vim.api.nvim_set_keymap('o', 'F', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true, inclusive_jump = true })<cr>", {})
vim.api.nvim_set_keymap('', 't', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>", {})
vim.api.nvim_set_keymap('', 'T', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>", {})

--system clipboard
nmap("<c-c>", "\"+y")
vmap("<c-c>", "\"+y")
nmap("<c-v>", "\"+p")
inoremap("<c-v>", "<c-r>+")
cnoremap("<c-v>", "<c-r>+")
--use <c-r> to insert original character without triggering things like auto-pairs
inoremap("<c-r>", "<c-v>")

--colors
vim.cmd[[colorscheme neon]]

-- Plugin setup

-- If windows, set sqlite3.dll path
if vim.fn.has("win32") == 1 then
    local config_path = vim.fn.stdpath("config")
    g.sqlite_clib_path = config_path:gsub("\\", "/") .. "/sqlite3.dll"
end

require"hardline".setup {}
require'hop'.setup {}
require"telescope".load_extension("frecency")

g.vimwiki_list = {{path = '~/Nextcloud/vimwiki', syntax = 'markdown', ext = '.md'}}

-- Mini
require("mini.surround").setup {
  -- Number of lines within which surrounding is searched
  n_lines = 20,

  -- Duration (in ms) of highlight when calling `MiniSurround.highlight()`
  highlight_duration = 500,

  -- Pattern to match function name in 'function call' surrounding
  -- By default it is a string of letters, '_' or '.'
  funname_pattern = '[%w_%.]+',

  -- Mappings. Use `''` (empty string) to disable one.
  mappings = {
    add = 'ys',           -- Add surrounding
    delete = 'ds',        -- Delete surrounding
    find = 'sf',          -- Find surrounding (to the right)
    find_left = 'sF',     -- Find surrounding (to the left)
    highlight = 'sh',     -- Highlight surrounding
    replace = 'cs',       -- Replace surrounding
    update_n_lines = 'sn' -- Update `n_lines`
  }
}

require("mini.cursorword").setup {delay = 100}
require("mini.pairs").setup {}

local cmp = require'cmp'
cmp.setup({
  snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
    end,
  },
  mapping = {
    ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' }, -- For luasnip users.
  }, {
    { name = 'buffer' },
  })
})

-- Neovide
g.neovide_remember_window_size = true
if vim.fn.has("win32") == 1 then
  opt.guifont = "Hack NF:h14"
else
  opt.guifont = "Hack Nerd Font Mono:h14"
end

